﻿#include <iostream>

int main()
{


	//zadanie 0
	srand(2137);

	const unsigned int size = 1000;

	int array[size] = {};

	for (int i = 0; i < size; i++) {
		if (i % 3 == 0) {
			array[i] = rand();
		}
		else {
			array[i] = -1;
		}
	}

	std::cout << "Zadanie 0" << std::endl;

	for (int i = 0; i < 10; i++) {
		std::cout << array[i] << std::endl;
	}
	std::cout << "..." << std::endl << std::endl;



	//Zadanie 1
	std::cout << "Zadanie 1" << std::endl;

	int sum = 0;


	for (int i = 0; i < size; i++) {
		sum = sum + array[i];
	}
	std::cout << "Suma Elementow Tablicy = " << sum << std::endl << std::endl;


	//Zadanie 2
	std::cout << "Zadanie 2" << std::endl;

	unsigned int count = 0;


	for (int i = 0; i < size; i++) {
		int x = array[i];

		if (6776 <= x && x <= 10101) {
			count++;
		}

	}
	std::cout << "Count = " << count << std::endl << std::endl;


	//Zadanie 3
	std::cout << "Zadanie 3" << std::endl;

	int min = 99999999;
	int max = 0;

	for (int i = 0; i < size; i++) {
		int x = array[i];

		if (x > max){
			max = x;
		}

		if (x > 0) {
			if (x < min) {
				min = x;
			}
		}
	}
	std::cout << "Min = " << min << std::endl << "Max = " << max << std::endl << std::endl;

	return 0;
}